# urls.py
from django.urls import path
from . import views
from .views import ArticleDetailView,ArticleListView

urlpatterns = [
    path('links', views.links, name='links'),
    path('article/<slug:slug>', ArticleDetailView.as_view(), name='article-detail'),
    path('', ArticleListView.as_view(), name='article-list'),
]