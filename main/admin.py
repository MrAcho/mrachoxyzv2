from django.contrib import admin
from .models import Article, ArticleCategories, SourceFile
# Register your models here.
admin.site.register(ArticleCategories)
admin.site.register(Article)
admin.site.register(SourceFile)