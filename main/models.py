from django.db import models
from ckeditor.fields import RichTextField
# Create your models here.
import os 
from django.conf import settings
import math
class ArticleCategories(models.Model):
    name = models.TextField(max_length=100)
    def __str__(self):
        return self.name

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

class SourceFile(models.Model):
     file = models.FileField()

     def __str__(self):
        return self.file.name
     
     def size(self):
         
         return convert_size(os.path.getsize(os.path.join(settings.MEDIA_ROOT,self.file.name)))
     
    

class Article(models.Model):
    title = models.TextField(max_length=200)
    date = models.DateTimeField(auto_now=True)
    category = models.ForeignKey(ArticleCategories, on_delete=models.CASCADE,null=True,blank=True)
    article = RichTextField()
    video = models.URLField(null=True,blank=True)
    slug = models.SlugField(default="article0")
    files = models.ManyToManyField(SourceFile)

    def __str__(self):
          return self.title

    def shrtDate(self):
         return "{}/{}/{}".format(self.date.day,self.date.month,self.date.year)