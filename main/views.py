from django.http import HttpResponse
from django.shortcuts import render,get_object_or_404
from .models import Article,ArticleCategories
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

def home(response):
	articles = Article.objects.order_by("-date")
	latestvid = articles.first().video
	return render(response, "main/home.html", {"object_list":articles,"latestvid":latestvid})

def links(response):
	return render(response, "main/links.html", {})

class ArticleDetailView(DetailView):
    model = Article
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
    
class ArticleListView(ListView):
    model = Article
    paginate_by = 100
    ordering = ["-date"]
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context